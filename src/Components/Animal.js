const { Component } = require("react");

class Animal extends Component{
    render(){
        const {kind} = this.props;
        return(
            <>
                <div>
                    {kind === "cat" ? <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/541117/cat.jpg" alt="cat"/> : <p>Meow not found :)</p>}
                </div>
            </>
        )
    }
}

export default Animal